import datetime
from threading import Timer

import grab_loader
import downloader_proxy

PATH_USED_PROXY = "data_files/used_proxy.txt"
PATH_USED_PROXY_BACK = "data_files/used_proxy_back.txt"
PATH_LAST_UPDATE_DATE = "data_files/last_update_date.txt"
load_threading = None


def __update_date_update__(date):
    file = open(PATH_LAST_UPDATE_DATE, 'w')
    file.write("{} {} {} {} {}".format(date.year, date.month, date.day, date.hour, date.minute))
    file.close()


def __load_date_update__():
    file = open(PATH_LAST_UPDATE_DATE, 'r')
    str = file.read().split(' ')
    if str != "":
        return datetime.datetime(int(str[0]), int(str[1]), int(str[2]), int(str[3]), int(str[4]))
    else:
        return datetime.datetime(2018, 10, 22, 4, 00)


def copy_to_file(file_path_1, file_path_2):
    file1 = open(file_path_1, 'r')
    file2 = open(file_path_2, 'w')
    file2.write(file1.read())
    file1.close()
    file2.close()


def delete_used_proxy_5_morning(interval=3600):  # проверяет каждый час в дефолту
    """
    :param interval: интервал проверки времени
    """
    global load_threading
    last_update_date = __load_date_update__()
    date_now = datetime.datetime.now()
    new_interval = interval
    date_delta = date_now - last_update_date

    if date_delta.days >= 1:
        copy_to_file(PATH_USED_PROXY, PATH_USED_PROXY_BACK)
        open(PATH_USED_PROXY, 'w').close()  # Очистка файла
        __update_date_update__(date_now)

        # Запуск потока на загрузку серверов
        if load_threading is not None:
            if not load_threading.isAlive():
                load_threading = downloader_proxy.start_load()
        else:
            load_threading = downloader_proxy.start_load()
    else:
        new_interval = 86400 - date_delta.total_seconds()
    Timer(new_interval, delete_used_proxy_5_morning).start()


if __name__ == '__main__':
    delete_used_proxy_5_morning()
    grab_loader.start_loader(postponed_start=0)
