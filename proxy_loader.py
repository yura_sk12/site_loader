import random

FILE_USED_PROXY = "data_files/used_proxy.txt"
FILE_PROXY_LIST = "data_files/checked_proxy.txt"
FILE_ERROR_PROXY = "data_files/proxy_error.txt"
DEBUG = True


def get_random_proxy(proxy_list):
    # формат ***   95.31.12.156:4145::SOCKS4::Russian Federation (RU)::3   ***
    rand_proxy = random.choice(proxy_list)
    proxy_list.remove(rand_proxy)
    return [rand_proxy[0], rand_proxy[1], rand_proxy[2]]


def add_used_proxy(dict_key):
    # На входе строка ip:port
    # на выходе ключи словаря
    file = open(FILE_USED_PROXY, 'a')
    file.write(dict_key + '\n')
    file.close()


def add_error_proxy(dict_key):
    file = open(FILE_ERROR_PROXY, 'a')
    file.write(dict_key + '\n')
    file.close()


def load_proxy_list(delete_used_proxy=True):
    # Вернет словарь из [95.31.12.156:4145, SOCKS4, Russian Federation (RU)]
    loaded_ip_list = []

    file_to_read = open(FILE_PROXY_LIST, 'r')
    used_file = open(FILE_USED_PROXY, 'r')

    readed_text = file_to_read.read().split('\n')
    used_proxy_list = used_file.read().split()

    file_to_read.close()
    used_file.close()

    for line in readed_text:
        if line:
            ip_port = line.split("::")[0]
            proxy_type = line.split("::")[1]
            country = line.split("::")[2]
            if delete_used_proxy and ip_port in used_proxy_list:
                print("Удален прокси: {}".format(ip_port))
                continue
            loaded_ip_list.append([ip_port, proxy_type, country])

    return loaded_ip_list
