from grab import Grab
from grab.error import *
import threading

lock = threading.Lock()
SECURITY_WITHOUT_CHECK = -2
SECURITY_UNKNOWN = -1
SECURITY_NONE = 0
SECURITY_SMALL = 1
SECURITY_GOOD = 2
SECURITY_IDEAL = 3
DEBUG = False
checked_proxy_list = []
total_checked = 0


def chunks(lst, chunk_size):
    """
    Разделения списка на несколько равных частей.
    :param lst: список
    :param chunk_size: число частей
    :return:
    """

    return [lst[i:i + int(len(lst)/chunk_size)] for i in range(0, len(lst), int(len(lst)/chunk_size))]


def read_checked_proxy():
    loaded_ip_list = []
    file_to_read = open('data_files/checked_proxy.txt', 'r')
    readed_text = file_to_read.read().split('\n')
    file_to_read.close()
    for line in readed_text:
        if line:
            loaded_ip_list.append(line.split("::")[0])
    return loaded_ip_list


def save_proxy_list_to_file(proxy_list):
    lock.acquire()
    file = open('data_files/checked_proxy.txt', 'a')
    for proxy in proxy_list:
        file.write("{}::{}::{}::{}\n".format(proxy[0], proxy[1], proxy[2], proxy[3]))
    lock.release()


def print_lock(str):
    lock.acquire()
    print(str)
    lock.release()


class ProxyChecker(threading.Thread):
    def __init__(self, proxy_list=[]):
        threading.Thread.__init__(self)
        self.site_whoer = "https://whoer.net/"
        self.site_azenv = "http://azenv.net/"
        self.g = Grab()                             # Grub() объект
        self.proxy_info = {}                        #
        self.proxy_list = proxy_list
        self.ip_port = ""                           #
        self.result_proxy_type = None               # Полученый в результате проверки proxy_type
        self.safe_proxy = SECURITY_WITHOUT_CHECK    # Хранить степень безопасности прокси просле проверки
        self.proxy_types = ["SOCKS4", "SOCKS5", "HTTP"]

    def run(self):
        global checked_proxy_list, total_checked
        checked_proxy_list = read_checked_proxy()
        len_for_write = 9  # Так как по одному адресу записывать в файл расточительно буде записываться по len_for_write
        list_for_write = []  # лист который будет записываться
        for ip_port in self.proxy_list:
            total_checked += 1
            if ip_port in checked_proxy_list:
                print_lock("Прокси {} уже использован".format(ip_port))
                continue
            res = self.check(ip_port)
            if res is not None:
                if res[2] == SECURITY_IDEAL:
                    list_for_write.append([ip_port, res[0], res[1], res[2]])
                    if len(list_for_write) >= len_for_write:
                        save_proxy_list_to_file(list_for_write)
                        checked_proxy_list = read_checked_proxy()
                        list_for_write.clear()
                    print_lock("Рабочий прокси № {} - {}:{}:{}:{}. Всего: {}".format(
                        len(checked_proxy_list), ip_port, res[0], res[1], res[2], total_checked))
                else:
                    print("Не безопасный прокси № {} - {}:{}:{}:{}. Всего: {}".format(
                        len(checked_proxy_list), ip_port, res[0], res[1], res[2], total_checked))
            if DEBUG:
                print_lock("Не рабочий прокси - {}".format(ip_port))
        if len(list_for_write):
            save_proxy_list_to_file(list_for_write)

    def __get_proxy_info__(self):
        try:
            self.proxy_info["country"] = self.g.doc.select(
                '//*[@id="content"]/div[7]/div[1]/div[1]/div[1]/div[1]/div[2]/dl/dd[1]/div/span').text()
            self.proxy_info["black_list"] = self.g.doc.select(
                '//*[@id="content"]/div[1]/div/div[2]/dl/dd[4]/span[2]').text()
            self.proxy_info["city"] = self.g.doc.select(
                '//*[@id="content"]/div[7]/div[1]/div[1]/div[1]/div[1]/div[2]/dl/dd[3]/span[2]').text()
            self.proxy_info["ip"] = self.g.doc.select('//*[@id="content"]/div[1]/div/div[1]/div/strong').text()
            self.proxy_info["browser"] = self.g.doc.select(
                '//*[@id="content"]/div[1]/div/div[1]/dl/dd[5]/span[2]').text()
            return self.proxy_info
        except DataNotFound:
            if DEBUG:
                print("Не найдена информация о прокси")
            return False
        return True

    def check(self, ip_port):
        """
        Проверит proxy
        Если работает вернет [type, country, security]
        Если не работает вернет None
        """
        ret = []
        self.proxy_info = {}
        self.result_proxy_type = None
        self.safe_proxy = SECURITY_WITHOUT_CHECK
        # Цикл по типам прокси
        for proxy_type in self.proxy_types:
            self.setting_grab(ip_port, proxy_type)
            country = self.get_country()
            if country:
                ret.append(proxy_type)
                ret.append(country)
                # Проверка безопасности только при использовании HTTP
                if proxy_type == "HTTP":
                    ret.append(self.check_security())
                else:
                    ret.append(SECURITY_IDEAL)
                return ret
        return None

    def get_country(self):
        if not self.load_page(self.site_whoer, repeat_cnt=0):
            return False
        if self.g.doc.code != 200:
            return False

        self.proxy_info = self.__get_proxy_info__()
        if self.proxy_info == {} or self.proxy_info is None:
            return False

        return self.proxy_info["country"]

    def check_security(self):
        """
        :return: -1 - Неизвесно
        :return: 0 - Удалённый сервер знает ваш IP, и знает, что вы используете прокси
        :return: 1 - Удалённый сервер не знает ваш IP, но знает, что вы используете прокси
        :return: 2 - Удалённый сервер знает, что вы используете прокси, и думает, что знает ваш IP, но он не ваш
        :return: 3 - Удалённый сервер не знает ваш IP, и у него нет прямых доказательств, что вы используете прокси
        """
        res = self.load_page(self.site_azenv)
        if self.g.doc.code != 200 or not res:
            self.safe_proxy = SECURITY_WITHOUT_CHECK
            return self.safe_proxy
        try:
            ans = self.g.doc.select("/html/body/pre").text()
        except DataNotFound:
            self.safe_proxy = SECURITY_WITHOUT_CHECK
            return self.safe_proxy
        http_x_forwarded_for = None
        http_via = None
        if "HTTP_X_FORWARDED_FOR" in ans:
            start_pos = ans.rfind("HTTP_X_FORWARDED_FOR = ")
            start_pos += len("HTTP_X_FORWARDED_FOR = ")
            http_x_forwarded_for = ans[start_pos:start_pos + 7]
        if "HTTP_VIA" in ans:
            http_via = not None
        if DEBUG:
            print("HTTP_X_FORWARDED_FOR = {}; HTTP_VIA = {}".format(http_x_forwarded_for, http_via))
        if http_x_forwarded_for is not None and http_via is not None:
            self.safe_proxy = SECURITY_NONE
        elif http_x_forwarded_for == "unknown":
            self.safe_proxy = SECURITY_GOOD
        elif http_x_forwarded_for is not None and http_via is None:
            self.safe_proxy = SECURITY_SMALL
        elif http_x_forwarded_for is None and http_via is None:
            self.safe_proxy = SECURITY_IDEAL
        else:
            self.safe_proxy = SECURITY_UNKNOWN
        return self.safe_proxy

    def setting_grab(self, ip_port, proxy_type, connect_timeout=3, timeout=5):
        self.g.setup(proxy=ip_port, proxy_type=proxy_type)
        self.g.setup(connect_timeout=connect_timeout, timeout=timeout)

    def __load_page__(self, url):
        try:
            self.g.go(url)
        except GrabConnectionError as conn_err:
            if DEBUG:
                print("CONNECTION REFUSE {}\n{}".format(conn_err.original_exc, self))
            return False
        except GrabTimeoutError as timeout_err:
            if DEBUG:
                print("TIMEOUT {}\n{}".format(timeout_err.original_exc, self))
            return False
        except GrabCouldNotResolveHostError as not_resolve:
            if DEBUG:
                print("{}\n{}".format(not_resolve.original_exc, self))
            return False
        except GrabNetworkError as http_code:
            if DEBUG:
                print("ОШИБКА - orig:{}\n{}".format(http_code.original_exc, self))
            return False
        except GrabTooManyRedirectsError as many_redir:
            if DEBUG:
                print("СЛИШКОМ МНОГО РЕДИРЕКТОВ - {} - orig:{}\n{}".format(self.g.response.headers,
                                                                           many_redir, self))
            return False
        return True

    def load_page(self, url, repeat_cnt=0):
        res = False
        while True:
            res = self.__load_page__(url)
            if res:
                break
            else:
                if repeat_cnt > 0:
                    if DEBUG:
                        print("Повторная попытка загрузить страницу с прокси")
                    repeat_cnt -= 1
                else:
                    break
        return res


def start_check(loaded_ip, max_threads=200):
    threads = []
    chunks_lists = chunks(list(loaded_ip), max_threads)
    for chunk in chunks_lists:
        threads.append(ProxyChecker(proxy_list=chunk))
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()


def read_loaded_ip():
    loaded_ip_ = []
    file_for_read_ip = open("data_files/loaded_ip.txt", 'r')
    readed = file_for_read_ip.read().split()
    file_for_read_ip.close()
    for ip_port in readed:
        if ip_port:
            loaded_ip_.append(ip_port)
    return loaded_ip_


if __name__ == '__main__':
    while True:
        start_check(read_loaded_ip())