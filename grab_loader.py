import threading
import time

import proxy_loader
import downloader_proxy

from Headers import *
from urls_for_connect import *
from grab import Grab, error

DEBUG = False
loaded_links = {"HTTP": 0, "HTTPS": 0, "SOCKS4": 0, "SOCKS5": 0, "PIC_MIR": 0}
lock = threading.Lock()

# todo - https://www.searchengines.ru/kak_raspoznat_block_bots.html
# todo - Все таки попытаться разобраться с параметрами которые может отправлять javascript например разрешение экрана
# так как разрешение переджается в куках можно их там поискать если есть то передавать если нет то сделать из
# стандартных имен
# todo https://compress.ru/article.aspx?id=9782
# todo TOR-https://jarroba.com/anonymous-scraping-by-tor-network/

class GrabLoader(threading.Thread):
    def __init__(self, proxy_list, ip_port='', proxy_type='', refer=''):
        threading.Thread.__init__(self)
        self.g = Grab()
        self.ip_port = ip_port
        self.proxy_type = proxy_type
        self.headers = ""
        self.country = ""
        self.page = ""
        self.referer = refer
        self.proxy_list = proxy_list

    def run(self):
        while len(self.proxy_list) > 0:
            lock.acquire()
            self.ip_port, self.proxy_type, self.country = proxy_loader.get_random_proxy(self.proxy_list)
            lock.release()
            self.proxy_type = self.proxy_type.replace(',', '')
            if self.proxy_type == "HTTPS":
                self.proxy_type = "HTTP"

            self.setting_grab()
            self.load_piccash()
            self.load_pic_mir()

    # При 20 000 прокси и 8 потоках есть примерно 90 секунд на загрузку
    # Поэтому бот может имитировать активность, т.е. смотреть не только одну картинку
    # random.expovariate(150) * 150 ** 2 - по моему мнению время нахождения на сайте эспонинцеально
    def load_piccash(self):
        time_for_load = random.expovariate(40) * 40 ** 2  # В среднем будет 150 сек
        tic_, toc_ = time.time(), time.time()
        res = False
        load_cnt = 0
        self.g.cookies.clear()

        while True:

            self.page = self.get_piccash_page()
            if self.load_page(self.page):
                load_cnt += 1
                res = True
                lock.acquire()
                print("Загружена ссылка: {}, "
                      "через прокси: {} "
                      "с типом: {}".format(self.page, self.ip_port, self.proxy_type))
                lock.release()

                time_to_sleep = random.expovariate(12) * 12 ** 2 + 3
                if time_to_sleep > time_for_load:
                    # Смысла ждать нету, так как следующая страница загружена не будет
                    # И сервер не поймет что я на ней сижу
                    break
                time.sleep(time_to_sleep)
                toc_ = time.time()
            else:
                break
            if toc_ - tic_ > time_for_load:
                break
        if res:
            # Статистика
            lock.acquire()
            proxy_loader.add_used_proxy(self.ip_port)
            loaded_links[self.proxy_type] += 1
            print(str(loaded_links) + "; Осталось серверов - " + str(len(self.proxy_list)), "; Загружено раз", load_cnt)
            lock.release()

    # При 20 000 прокси есть примерно 90 секунд на загрузку
    # Поэтому бот может имитировать активность, т.е. смотреть не только одну картинку
    # random.expovariate(150) * 150 ** 2 - по моему мнению время нахождения на сайте эспонинцеально
    def load_pic_mir(self):
        time_for_load = random.expovariate(40) * 40 ** 2  # В среднем будет 150 сек
        tic_, toc_ = time.time(), time.time()
        res = False
        load_cnt = 0
        self.g.cookies.clear()

        while True:
            self.page = self.get_pic_mir_page()
            flag = False

            # Загружаем только через прокси стран снг
            for cntry in downloader_proxy._russian_and_CIS_countries:
                if cntry in self.country:
                    flag = True
            if flag:
                if self.load_page(self.page):
                    load_cnt += 1
                    res = True
                    try:
                        self.g.submit()
                    except Exception as e:
                        print("Ошибка при отправке submit - ", e)
                        return
                    lock.acquire()
                    print("Загружена ссылка: {}, "
                          "через прокси: {} "
                          "с типом: {}".format(self.page, self.ip_port, self.proxy_type))
                    lock.release()

                    time_to_sleep = random.expovariate(12) * 12 ** 2 + 3
                    if time_to_sleep > time_for_load:
                        # Смысла ждать нету, так как следующая страница загружена не будет
                        # И сервер не поймет что я на ней сижу
                        break
                    time.sleep(time_to_sleep)
                toc_ = time.time()
            else:
                break
            if toc_ - tic_ > time_for_load:
                break

        if res:
            # Статистика
            lock.acquire()
            proxy_loader.add_used_proxy(self.ip_port)
            loaded_links["PIC_MIR"] += 1
            print(str(loaded_links) + "; Осталось серверов - " + str(len(self.proxy_list)), "; Загружено раз", load_cnt)
            lock.release()

    def get_grab(self):
        return self.g

    def generate_header(self):
        self.headers = headers
        self.headers['Referer'] = refer_sites__header[random.randint(0, len(refer_sites__header) - 1)]
        self.headers['Connection'] = connection_header[random.randint(0, len(connection_header) - 1)]
        self.headers['User-Agent'] = user_agent__header[random.randint(0, len(user_agent__header) - 1)]
        self.headers['Accept'] = accept_header[random.randint(0, len(accept_header) - 1)]
        self.headers['Accept-Language'] = accept_language__header[random.randint(0, len(accept_language__header) - 1)]

    def generate_refer(self):
        """
        По-умолчанию, Grab сам настраивает несколько HTTP-заголовков:
        Accept, Accept-Language, Accept-Charset, Keep-Alive и User-Agent
        Поэтому можно сгенерировать только refer
        """
        self.referer = refer_sites__header[random.randint(0, len(refer_sites__header) - 1)]
        return self.referer

    def setting_grab(self, debug=DEBUG, connect_timeout=15, timeout=20):
        self.g.setup(proxy=self.ip_port, proxy_type=self.proxy_type, debug=debug)
        self.g.setup(connect_timeout=connect_timeout, timeout=timeout)
        if self.referer != '':
            self.g.setup(referer=self.referer)
        if self.headers != '':
            self.g.setup(headers=self.headers)

    def load_page(self, url):
        try:
            self.g.go(url)
        except error.GrabConnectionError as conn_err:
            if DEBUG:
                print("CONNECTION REFUSE {}\n{}".format(conn_err.original_exc, self))
            return False
        except error.GrabTimeoutError as timeout_err:
            if DEBUG:
                print("TIMEOUT {}\n{}".format(timeout_err.original_exc, self))
            return False
        except error.GrabCouldNotResolveHostError as not_resolve:
            if DEBUG:
                print("{}\n{}".format(not_resolve.original_exc, self))
            return False
        except error.GrabNetworkError as http_code:
            if DEBUG:
                print("ОШИБКА НОМЕР - {} - orig:{}\n{}".format(self.g.response.headers, http_code.original_exc, self))
            return False
        except error.GrabTooManyRedirectsError as many_redir:
            if DEBUG:
                print("СЛИШКОМ МНОГО РЕДИРЕКТОВ - {} - orig:{}\n{}".format(self.g.response.headers,
                                                                           many_redir, self))
            return False
        return True

    @staticmethod
    def get_piccash_page():
        return random.choice(all_url)

    @staticmethod
    def get_pic_mir_page():
        return random.choice(urls_pic_mir_simps_pron)

    def __str__(self):
        return "ip:{}, type:{}, header:{}\n".format(self.ip_port, self.proxy_type, self.headers)


def chunks(lst, chunk_size):
    """
    Разделения списка на несколько равных частей.
    :param lst: список
    :param chunk_size: число частей
    :return:
    """

    return [lst[i:i + int(len(lst)/chunk_size)] for i in range(0, len(lst), int(len(lst)/chunk_size))]


def start_loader(max_threads=20, postponed_start=180):
    proxy_list = proxy_loader.load_proxy_list()
    threads = []
    chunks_lists = chunks(list(proxy_list), max_threads)
    for chunk in chunks_lists:
        threads.append(GrabLoader(chunk))
    for thread in threads:
        thread.start()
        time.sleep(random.random()*postponed_start)


if __name__ == '__main__':
    loader = GrabLoader(proxy_loader.load_proxy_list()[:3])
    loader.start()

    # Расчет производительности
    tic = time.time()
    loader.join()
    toc = time.time()
    print("Прошло времени {}, за 1 день будет пройдено {}, для 20к серверов нужно {} потоков".format(
        toc - tic, 24*60*60/((toc - tic)/3), 20000/(24*60*60/((toc - tic)/3))))
