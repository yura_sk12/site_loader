import random
import threading
import time

import requests
import os
import re
import proxy_checker

import selenium
from bs4 import BeautifulSoup
from grab import Grab, DataNotFound
from selenium import webdriver

_tier1_country = ["United States", "Canada", "United Kingdom", "Australia", "New Zealand"]
_tier2_country = ["Austria", "Belgium", "Denmark", 'Finland', 'France', 'Germany', 'Greece', 'Ireland', 'Italy',
                  'Luxembourg', 'Netherlands', 'Norway', 'Portugal', 'Spain', 'Sweden', 'Switzerland']
_tier3_country = ['Cyprus', 'Czech Republic', 'Estonia', 'Hungary', 'Israel', 'Latvia', 'Lithuania', 'Poland', 'Russia',
                  'Russian Federation', 'Slovakia', 'Slovenia']
_russian_and_CIS_countries = ['Russia', 'Russian Federation', 'Azerbaijan', 'Armenia', 'Belarus', 'Kazakstan',
                              'Kyrgyzstan', 'Malawi', 'Tajikistan', 'Uzbekistan']
driver_path = "drivers/chromedriver.exe"

lock = threading.Lock()
function_list = []
regular_for_ip = r'\d+\.\d+\.\d+\.\d+:\d+'
DEBUG = True
used_proxy = open('data_files/used_proxy.txt', 'r').read().split()
proxy_set_all = set()
loaded_ip = []


def read_loaded_ip():
    loaded_ip_ = []
    file_for_read_ip = open("data_files/loaded_ip.txt", 'r')
    readed = file_for_read_ip.read().split("\n")
    file_for_read_ip.close()
    for ip_port in readed:
        if ip_port:
            loaded_ip_.append(ip_port)
    return loaded_ip_


def save_proxy_list_to_file(proxy_list):
    lock.acquire()
    file = open('data_files/loaded_ip.txt', 'a')
    for ip_port in proxy_list:
        if ip_port not in loaded_ip:
            file.write(ip_port + "\n")
            loaded_ip.append(ip_port)
    lock.release()


def __find_ip_on_page__(text_where_search):
    """
    Поиск Ip по регулярному выражению в тексте
    """
    return re.findall(regular_for_ip, text_where_search)


def download_from_us_proxy_org():
    proxy_list = []
    site_name = "https://www.us-proxy.org/"
    try:
        r = requests.get(site_name)
        if r.status_code != 200:
            print('www.us-proxy.org недоступен, код ошибки ', r.request)
            return False

        soup = BeautifulSoup(r.text, 'html.parser')
        table_with_proxy = soup.find('tbody')
        tr_with_proxy_row = table_with_proxy.find_all('tr')
        for row in tr_with_proxy_row:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            data = ([ele for ele in cols if ele])  # Get rid of empty values
            ip = data[0]
            port = data[1]
            proxy_list.append(ip + ":" + port)
    except Exception as e:
        print("Ошибка в https://www.us-proxy.org/ ", e)
        if not len(proxy_list):
            return False
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с https://www.us-proxy.org/")


def download_from_socks_proxy_net():
    proxy_list = []
    site_name = "https://www.socks-proxy.net/"
    r = requests.get(site_name)
    if r.status_code != 200:
        print('www.socks-proxy.net недоступен, код ошибки ', r.status_code)
        return False
    try:
        soup = BeautifulSoup(r.text, 'html.parser')
        table_with_proxy = soup.find('tbody')
        tr_with_proxy_row = table_with_proxy.find_all('tr')
        for row in tr_with_proxy_row:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            data = ([ele for ele in cols if ele])  # Get rid of empty values
            ip = data[0]
            port = data[1]
            proxy_list.append(ip + ":" + port)
    except Exception as e:
        print("Ошибка загрузки с https://www.socks-proxy.net/ ", e)
        if not len(proxy_list):
            return False
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с https://www.socks-proxy.net/")


def download_from_sslproxies_org():
    proxy_list = []
    site_name = "https://www.sslproxies.org/"
    try:
        r = requests.get(site_name)
        if r.status_code != 200:
            print('www.sslproxies.org недоступен, код ошибки ', r.status_code)
            return False

        soup = BeautifulSoup(r.text, 'html.parser')
        table_with_proxy = soup.find('tbody')
        tr_with_proxy_row = table_with_proxy.find_all('tr')
        for row in tr_with_proxy_row:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            data = ([ele for ele in cols if ele])  # Get rid of empty values
            ip = data[0]
            port = data[1]
            proxy_list.append(ip + ":" + port)
    except Exception as e:
        print("ошибка загрузки https://www.sslproxies.org/ ", e)
        if not len(proxy_list):
            return False
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с https://www.sslproxies.org/")


# Ручная загрузка файлов
# https://awmproxy.com/freeproxy.php
# http://www.live-socks.net/
# http://www.socks24.org/
def download_from_files_from_iplist():
    proxy_list = []
    for filename in os.listdir('iplist/'):
        with open('iplist/' + filename, 'r') as file_handler:
            proxy_list += __find_ip_on_page__(file_handler.read())
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с http://www.live-socks.net/, and http://www.socks24.org/, "
          "https://awmproxy.com/freeproxy.php")


def download_from_hidemyna_me():
    url_for_tier_1 = "https://hidemyna.me/ru/proxy-list/?country=AUCANZGBUS&anon=4&start="
    url_for_all_countries = "https://hidemyna.me/ru/proxy-list/?anon=4&start="
    url_for_tier_2 = "https://hidemyna.me/ru/proxy-list/?country=ATBECZDKFIFRDEGRIEITNLNOPTESSECH&anon=4&start="
    first_name = url_for_all_countries
    last_name = "#list"
    page = 0  # кратно 64
    page_cnt = 500
    proxy_hidemyna = first_name + str(page) + last_name

    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    options.add_argument("--window-size=%s" % "1920,1080")
    driver = webdriver.Chrome(executable_path=driver_path, options=options)
    if DEBUG:
        print("Идет загрузка прокси c https://hidemyna.me")
    try:
        proxy_list = []
        driver.get(proxy_hidemyna)
        time.sleep(15)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        table_proxy_t = driver.find_element_by_class_name("proxy__t")
        table_tbody = table_proxy_t.find_element_by_tag_name("tbody")
        tr_list = table_tbody.find_elements_by_tag_name("tr")
        for tr in tr_list:
            list_data = tr.text.split("\n")
            ip = list_data[0].replace(' ', ':')
            proxy_list.append(ip)

            if DEBUG:
                print("Загружен прокси - {}: сайт-{}".format(ip, "https://hidemyna.me"))
        save_proxy_list_to_file(proxy_list)

        # После прохода через ddos защиту проходим по всем страницам
        for current_page in range(page_cnt):
            proxy_list = []
            if DEBUG:
                print("Чтение ", current_page + 2, " страницы")
            page += 64
            proxy_hidemyna = first_name + str(page) + last_name
            driver.get(proxy_hidemyna)
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            table_proxy_t = driver.find_element_by_class_name("proxy__t")
            table_tbody = table_proxy_t.find_element_by_tag_name("tbody")
            tr_list = table_tbody.find_elements_by_tag_name("tr")
            if len(tr_list) == 0:
                if DEBUG:
                    print("Прочитаны все страници")
                break
            for tr in tr_list:
                list_data = tr.text.split("\n")
                ip = list_data[0].replace(' ', ':')
                proxy_list.append(ip)

                if DEBUG:
                    print("Загружен прокси - {}: сайт - {}".format(ip, "https://hidemyna.me"))
            time.sleep(random.random()*40 + 10)
            save_proxy_list_to_file(proxy_list)
    except Exception as e:
        driver.quit()
        print("Ошибка при загрузке https://hidemyna.me/ ", e)
        return False
    driver.quit()
    print("Успешно загружено с https://hidemyna.me/")


# todo закрыли доступ
def download_from_spys_one():
    proxy_list = []
    site_country = "http://spys.one/en/proxy-by-country/"
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    options.add_argument("--window-size=%s" % "1920,1080")
    driver = webdriver.Chrome(executable_path=driver_path, options=options)

    try:
        driver.get(site_country)
        spy1x_list = driver.find_elements_by_class_name("spy1x")

        # Загрузка ссылок на страны
        for index in range(len(spy1x_list)):
            country = spy1x_list[index].text
            country = country.split("\n")[1]
            country_full_name = ' '.join(country.split(' ')[:-2])
            country_reduce = ' '.join(country.split(' ')[-2:-1])
            spy1x_list[index].click()
            __download_from_spys_one__(proxy_list, driver, country_reduce, country_full_name)
            driver.get(site_country)
            spy1x_list = driver.find_elements_by_class_name("spy1x")
    except Exception as e:
        driver.quit()
        print("Ошибка загрузки с spys.one ", e)
        if not len(proxy_list):
            return False
    driver.quit()
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с http://www.spys.one")


def __download_from_spys_one__(proxy_list, driver, rename_name, rename_new_name):
    spy1_class = driver.find_element_by_class_name("spy1")
    xpp_id = spy1_class.find_element_by_id("xpp")
    options_xpp = xpp_id.find_elements_by_tag_name("option")
    options_xpp[-1].click()  # Выбор вывода кол-ва элментов

    # После загрузки большего числа элементов нужно заного парсить страницу
    spy1_class = driver.find_element_by_class_name("spy1")
    xf1_id = spy1_class.find_element_by_id("xf1")
    options_xf1 = xf1_id.find_elements_by_tag_name("option")
    options_xf1[-1].click()  # выбор уровня анонимности

    # Делаем выборку из таблицы
    spy1xx_list = driver.find_elements_by_class_name("spy1xx")[1:]
    spy1x_list = driver.find_elements_by_class_name("spy1x")[1:]
    spy1_list = spy1xx_list + spy1x_list
    for spy in spy1_list:
        spy_td = spy.find_elements_by_tag_name("td")
        proxy_type = spy_td[1].text
        ip = spy_td[0].text.split(' ')[1].split(":")[0]
        port = spy_td[0].text.split(' ')[1].split(":")[1]
        country = spy_td[3].text.replace(rename_name, rename_new_name)

        proxy_list.append(ip + ':' + port)
        if DEBUG:
            print("Загружен proxy - ", proxy_type + ";" + ip + ':' + port + ";" + country, " сайт - http://spys.one")

    return True


def download_from_openproxy_space():
    reg = re.compile('[^a-zA-Z0-9 ]')
    site_country = "https://openproxy.space/country/"
    sites = []
    proxy_list = []

    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    options.add_argument("--window-size=%s" % "1920,1080")
    driver = webdriver.Chrome(executable_path=driver_path, options=options)
    try:
        driver.get(site_country)
        select_block = driver.find_element_by_class_name("select-block")
        links = select_block.find_elements_by_tag_name("a")
        for link in links:
            sites.append(link.get_property("href"))

        for site in sites[1:]:
            loaded_proxy = 0
            driver.get(site)
            start_index = 0

            # Пока не загрузили все или 2000 проксей
            while True:

                # Пока нажимается кнопка "больше"
                while True:
                    proxy_list = []
                    proxy_items = driver.find_element_by_class_name("proxy-items")
                    proxy_items_socks = proxy_items.find_elements_by_class_name("proxy-item")

                    # Проходим по всем загруженным на странице прокси
                    for proxy in proxy_items_socks:
                        country = proxy.find_element_by_class_name("country-name").text
                        ip_port = proxy.find_element_by_class_name("item-ip-port").text
                        anon = proxy.find_element_by_class_name("anon").text
                        proxy_type = proxy.find_element_by_class_name("item-type").find_elements_by_tag_name("a")[
                            0].text.upper()
                        proxy_type = reg.sub('', proxy_type)
                        # proxy_list.append("{}::{}::{}::{}".format(country, ip_port, anon, proxy_type))

                        # Удаляем все видимые прокси
                        driver.execute_script("""
                            var element = arguments[0];
                            element.parentNode.removeChild(element);
                            """, proxy)
                        if anon.lower() == "Elite".lower():
                            proxy_list.append(ip_port)
                            if DEBUG:
                                print("Загружен прокси - {}: сайт - {}".format(ip_port, "https://openproxy.space"))
                            loaded_proxy += 1
                        else:
                            if DEBUG:
                                print("Пропещен прокси - {} - так как не анонимен: тип - ".format(ip_port, anon))
                    if DEBUG:
                        print("Загружено прокси серверов - ", loaded_proxy)
                    more = driver.find_elements_by_class_name("more")
                    if not len(proxy_items_socks):
                        break
                    save_proxy_list_to_file(proxy_list)
                if len(more):
                    try:
                        more[0].click()
                        time.sleep(random.random() * 20 + 5)
                        if not len(proxy_items.find_elements_by_class_name("proxy-item")):
                            break
                    except selenium.common.exceptions.WebDriverException:
                        print("Не удалось нажать кнопку load more")
                        break
                else:
                    break
                if loaded_proxy > 2000:
                    break
            loaded_proxy += 1
    except Exception as e:
        driver.quit()
        print("Не загружено с openproxy.space ", e)
        if not len(proxy_list):
            return False
    driver.quit()
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с openproxy.space")


# todo похоже что тоже закрыли доступ
def download_from_free_proxy_cz():
    proxy_list = []
    sites = ["http://free-proxy.cz/ru/proxylist/country/US/all/ping/level1/",  # США
             "http://free-proxy.cz/ru/proxylist/country/CA/all/ping/level1/",  # Канада
             "http://free-proxy.cz/ru/proxylist/country/GB/all/ping/level1/",  # Великобритании
             "http://free-proxy.cz/ru/proxylist/country/AU/all/ping/level1/",  # Автралия
             "http://free-proxy.cz/ru/proxylist/country/NZ/all/ping/level1/",  # Новая зеландия
             ]
    num_site = 0
    proxy_list = []
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    options.add_argument("--window-size=%s" % "1920,1080")
    driver = webdriver.Chrome(executable_path=driver_path, options=options)
    try:
        for site in sites:
            page = 1
            driver.get(site + str(page))
            paginator = driver.find_elements_by_class_name("paginator")
            if len(paginator):
                max_pages = int(paginator[0].find_elements_by_tag_name("a")[-2].text)
            else:
                max_pages = 1
            while True:
                proxy_table = driver.find_element_by_id("proxy_list")
                tr_list = proxy_table.find_elements_by_tag_name('tr')
                for tr in tr_list:
                    text_split = tr.text.split("\n")
                    if len(text_split) > 2:
                        ip = text_split[0].split(" ")[0]
                        port = text_split[0].split(" ")[1]
                        # proxy_type = text_split[0].split(" ")[2]
                        # country = _tier1_country[num_site]
                        proxy_list.append(ip + ":" + port)

                        if DEBUG:
                            print("Загружен прокси - {}; {}".format(ip + ":" + port, proxy_list[-1]))
                page += 1
                if page > max_pages:
                    break
                else:
                    if DEBUG:
                        print("Страница ", page)
                    time.sleep(1)
                    driver.get(site + str(page))
            time.sleep(5)
            num_site += 1
    except Exception as e:
        driver.quit()
        print("Не загружено с free-proxy.cz ", e)
        if not len(proxy_list):
            return False
    driver.quit()
    save_proxy_list_to_file(proxy_list)
    print("Загружено с free-proxy.cz")


def download_from_checkerproxy_net():
    proxy_list = []
    site_all_proxy = "https://checkerproxy.net/getAllProxy"
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    options.add_argument("--window-size=%s" % "1920,1080")
    driver = webdriver.Chrome(executable_path=driver_path, options=options)

    try:
        driver.get(site_all_proxy)
        class_main = driver.find_element_by_class_name("main")
        li = class_main.find_element_by_tag_name('li')
        li.click()

        time.sleep(15)
        select_type = driver.find_element_by_class_name("select_type")
        selects = select_type.find_elements_by_tag_name("select")[-1]
        _options = selects.find_elements_by_tag_name("option")
        for country in _options:
            country.click()
            time.sleep(3)
            __download_from_checkerproxy_net(proxy_list, driver)
    except Exception as e:
        driver.quit()
        print("Не загружено с checkerproxy.net ", e)
        if not len(proxy_list):
            return False
    driver.quit()
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с checkerproxy.net")


def __download_from_checkerproxy_net(proxy_list, driver):
    resultTable = driver.find_element_by_id("resultTable")
    resultTable_tbody = resultTable.find_element_by_tag_name("tbody")
    tr_list = resultTable_tbody.find_elements_by_tag_name("tr")
    for tr in tr_list:
        td = tr.find_elements_by_tag_name("td")
        ip_port = td[0].text
        # country = td[1].text
        # proxy_type = td[2].text
        proxy_list.append(ip_port)

        if DEBUG:
            print("Загружен прокси - {} - {}: сайт - {}".format(ip_port, proxy_list[-1], "https://checkerproxy.net"))


def download_from_proxies_su():
    site = "http://proxies.su/?anonymity_type=elite&country=&page="
    page = 1
    g = Grab()
    try:
        g.setup(connect_timeout=30, timeout=40)
        g.go(site + str(page))
        paginator = g.doc.select('//*[@id="wrap"]/div[1]/div/div/div[1]/div[3]/div/ul')
        max_page = int(paginator.select('//*[@id="wrap"]/div[1]/div/div/div[1]/div[3]/div/ul/li[14]').text())
        for i in range(1, max_page + 1):
            proxy_list = []
            time.sleep(random.random()*80+20)
            g.go(site + str(i))
            table_body = g.doc.select('//*[@id="wrap"]/div[1]/div/div/div[1]/div[2]/div/table/tbody')
            soup = BeautifulSoup(table_body.html(), 'html.parser')
            tr_list = soup.find_all('tr')
            for tr in tr_list:
                res = tr.text.split()
                ip = res[2]
                port = res[3]
                proxy_type = res[4].upper()
                country = res[6]
                proxy_list.append(ip + ":" + port)
                if DEBUG:
                    print("Загружен прокси {}, типа {}, из {}, сайт - {}, страница {} из {}".format(
                        ip + ":" + port, proxy_type, country, "http://proxies.su", i, max_page))
            save_proxy_list_to_file(proxy_list)
    except DataNotFound:
        print("DataNotFound http://proxies.su/")
        return False
    except Exception as e:
        print("Не загружено с http://proxies.su/ ", e)
        return False
    print("Успешно загружено с http://proxies.su/")


def download_from_proxyfire():
    site = "http://www.proxyfire.net/forum/forumdisplay.php?f=14"
    forum_page = "http://www.proxyfire.net/forum/showthread.php?t="
    g = Grab()
    try:
        g.go(site)
        time.sleep(random.random()*20+5)
        soup = BeautifulSoup(g.doc.select('//*[@id="threadbits_forum_14"]').html(), 'html.parser')
        max_forums = 10
        for i in range(1, max_forums+1):
            a_link = soup.contents[0].contents[i].contents[5].contents[1].contents[3]
            # Если в заголовке L1 или Elite
            if "L1" in a_link.text or 'Elite' in a_link.text:
                proxy_list = []
                # Получаем id страницы для перехода
                id_page = a_link.get('id').split('_')[2]
                g.go(forum_page+id_page)
                time.sleep(random.random()*20+5)
                proxy_list = __find_ip_on_page__(g.doc.body.decode('utf-8'))
                save_proxy_list_to_file(proxy_list)
    except Exception as e:
        print("Не загружено с http://www.proxyfire.net/", e)
        return False
    print("Успешно загружено с http://www.proxyfire.net/")


def download_from_worldproxy():
    proxy_list = []
    site = "https://worldproxy.info/?q=uggc%3A%2F%2Fjjj.cnynprzbba.pbz%2Fcz%2Fsbehz%2F60&hl=0011110000"
    g = Grab()
    try:
        g.go(site)
        proxy_list = __find_ip_on_page__(g.doc.body.decode('utf-8'))
    except Exception as e:
        print("Не загружено с http://proxy.guajicun.com/ ", e)
        if not len(proxy_list):
            return False
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с http://proxy.guajicun.com/")


def download_from_1337_16mb():
    proxy_list = []
    site = "http://1337.16mb.com/"
    g = Grab()
    try:
        g.go(site)
        proxy_list = __find_ip_on_page__(g.doc.body.decode('utf-8'))
    except Exception as e:
        print("Не загружено с http://1337.16mb.com/ ", e)
        if not len(proxy_list):
            return False
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с http://1337.16mb.com/")


def download_from_proxy_guajicun():
    proxy_list = []
    site = "http://proxy.guajicun.com/freesharing/http/1/"
    g = Grab()
    try:
        g.go(site)
        proxy_list = __find_ip_on_page__(g.doc.body.decode('utf-8'))
    except Exception as e:
        print("Не загружено с http://proxy.guajicun.com/ ", e)
        if not len(proxy_list):
            return False
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с http://proxy.guajicun.com/")


def download_from_freshnewproxies24():
    proxy_list = []
    site = "http://www.freshnewproxies24.top/feeds/posts/default"
    g = Grab()
    try:
        g.go(site)
        proxy_list = __find_ip_on_page__(g.doc.body.decode('utf-8'))
    except Exception as e:
        print("http://www.freshnewproxies24.top/ ", e)
        if not len(proxy_list):
            return False
    save_proxy_list_to_file(proxy_list)
    print("Успешно загружено с http://www.freshnewproxies24.top/")


def add_all_func():
    function_list.append(download_from_us_proxy_org)
    function_list.append(download_from_socks_proxy_net)
    function_list.append(download_from_sslproxies_org)
    function_list.append(download_from_files_from_iplist)
    function_list.append(download_from_hidemyna_me)
    function_list.append(download_from_spys_one)
    function_list.append(download_from_openproxy_space)
    function_list.append(download_from_free_proxy_cz)
    function_list.append(download_from_checkerproxy_net)
    function_list.append(download_from_proxies_su)
    function_list.append(download_from_proxyfire)
    function_list.append(download_from_worldproxy)
    function_list.append(download_from_1337_16mb)
    function_list.append(download_from_proxy_guajicun)
    function_list.append(download_from_freshnewproxies24)


def __start_load__():
    add_all_func()
    thread_list = []
    for func in function_list[::-1]:
        thread_list.append(threading.Thread(target=func))
    for thread in thread_list:
        time.sleep(2)
        thread.start()
    for thread in thread_list:
        thread.join()
    proxy_checker.start_check(read_loaded_ip())


def start_load():
    t = threading.Thread(target=__start_load__)
    t.start()
    return t


# Перед добавлением Ip идет проверка о наличии с Loaded_ip,
# после сохр. в файл ip пишется в эту переменную
loaded_ip = read_loaded_ip()  # !Не удалять
if __name__ == '__main__':
    start_load()
    proxy_checker.start_check(read_loaded_ip())
