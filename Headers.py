import random
from copy import deepcopy

import requests
from bs4 import BeautifulSoup

headers = {'Accept-Language': 'en-US,en;q=0.5',
           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Referer': 'multporn.net/pictures',
           'Connection': 'keep-alive'}

# https://theporndude.com/ru
refer_sites__header = ["http://www.layxes.ucoz.net",
                       'http://www.rule34.paheal.net',
                       'http://www.r34anim.com',
                       'http://www.multporn.net/pictures',
                       'http://www.instantfap.com/category/rule34',
                       'https://www.hdsex.club/rule-34-2b-search',
                       'http://www.aye2015.org/video-irkd99a/minecraft_rule_34_porn_machinima_by_slipperyt',
                       'https://www.rule34hentai.net/',
                       'https://www.xnxx.com/tags/cartoon',
                       'https://www.forumophilia.com/forum24.html',
                       'https://www.rabbitsreviews.com/c36/Cartoons.html',
                       'https://www.forum.hentai2read.com/',
                       'https://www.hentai2read.com/hentai-list/',
                       'https://www.hentai2read.com/hentai-list/all/any/all/top-rating',
                       'https://www.pornolandia.xxx/',
                       'https://www.theporndude.com/ru/1335/eroticbeauties',
                       'https://www.theporndude.com/ru/660/coedcherry',
                       'https://www.theporndude.com/ru/353/420chan',
                       'http://www.isexychat.com/',
                       'https://www.freechatnow.com/',
                       'https://www.chat-avenue.com/',
                       'http://www.omegle.com/',
                       'https://www.theporndude.com/ru/700/chatroulette',
                       'https://www.theporndude.com/ru/1327/uselessjunk',
                       'https://www.8muses.com/comix/',
                       'http://www.multporn.net/',
                       'https://www.svscomics.com/',
                       'https://www.theporndude.com/ru/1556/porncomixonline',
                       'https://www.theporndude.com/ru/1359/xyzcomics',
                       'https://www.theporndude.com/ru/1553/nxt-comics',
                       'https://www.theporndude.com/ru/3010/alladultx',
                       'https://www.xhamster.com/categories/arab',
                       'http://www.pornbb.org/forum',
                       'https://www.forumophilia.com/',
                       'http://www.forum.phun.org/index.php',
                       'http://www.kitty-kats.net/',
                       'https://www.eroticity.net/forum.php',
                       'http://www.vintage-erotica-forum.com/',
                       'http://www.intporn.org/',
                       'http://www.nudecelebforum.com/',
                       'https://www.theporndude.com/ru/2526/dickflash',
                       'https://www.theporndude.com/ru/28/xnxx',
                       'https://www.vk.com/feed']
accept_language__header = ['fr-CH',
                           'fr;q=0.9',
                           'en;q=0.8',
                           'de;q=0.7',
                           'q=0.5',
                           'en-US,en;q=0.5',
                           'en;q=0.5',
                           'en-US']

user_agent__header = []
connection_header = ['Keep-Alive',
                     'Keep-Alive',
                     'Keep-Alive',
                     'Keep-Alive',
                     # 'Upgrade',
                     'Keep-Alive',
                     'Keep-Alive']
accept_header = ['text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8']


def load_user_agent(update=False):
    global user_agent__header
    user_agent_site = "https://developers.whatismybrowser.com/useragents/explore/hardware_type_specific/computer/"
    page_cnt = 1000
    if update:
        print("Идет загрузка юзерагентов")
        file = open("data_files/user_agents.txt", 'w')
        for index in range(1, page_cnt):
            print("Страница ", index, ' из ', page_cnt)
            r = requests.get(user_agent_site + str(index))
            if r.status_code == 200:
                data = r.text
                soup = BeautifulSoup(data, 'html.parser')
                table = soup.find('table',
                                  attrs={'class': 'table table-striped table-hover table-bordered table-useragents'})
                tbody = table.find('tbody')
                td_list = tbody.find_all('td', attrs={'class': 'useragent'})
                for td in td_list:
                    file.write(td.find('a').text + '\n')
            else:
                print("Не удалось обновить юзерагентов")

        file.close()
    file = open("data_files/user_agents.txt", 'r')
    user_agent__header = file.read().split('\n')[:-1]


load_user_agent()


def generate_random_header(self):
    header = deepcopy(headers)
    header['Referer'] = refer_sites__header[random.randint(0, len(refer_sites__header) - 1)]
    header['Connection'] = connection_header[random.randint(0, len(connection_header) - 1)]
    header['User-Agent'] = user_agent__header[random.randint(0, len(user_agent__header) - 1)]
    header['Accept'] = accept_header[random.randint(0, len(accept_header) - 1)]
    header['Accept-Language'] = accept_language__header[random.randint(0, len(accept_language__header) - 1)]
    return header


if __name__ == '__main__':
    load_user_agent(update=True)